package br.com.brunomagnum.cliente.repositories;

import br.com.brunomagnum.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
