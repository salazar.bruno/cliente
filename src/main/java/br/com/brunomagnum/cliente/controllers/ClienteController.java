package br.com.brunomagnum.cliente.controllers;

import br.com.brunomagnum.cliente.models.Cliente;
import br.com.brunomagnum.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping("/{id}")
    public Cliente buscaClientePorId(@PathVariable(name = "id") Integer id) {
        try {
            return clienteService.buscaClientePorId(id);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<Cliente> criaCliente(@RequestBody @Valid Cliente cliente) {
        try {
            return ResponseEntity.status(201).body(clienteService.criaCliente(cliente));
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
